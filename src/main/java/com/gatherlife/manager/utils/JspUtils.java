package com.gatherlife.manager.utils;

import java.net.URLDecoder;
import java.util.UUID;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.xson.common.object.XCO;
import org.xson.tangyuan.cache.CacheComponent;
import org.xson.tangyuan.cache.TangYuanCache;

import com.gatherlife.manager.Constant;


public class JspUtils {
	
	private static Logger	logger	= Logger.getLogger(JspUtils.class);
	
	/**
	 * 获取Token
	 */
	public static String getToken(HttpServletRequest request) {
		Cookie cookies[] = request.getCookies();
		Cookie sCookie = null;
		for (int i = 0; i < cookies.length; i++) {
			sCookie = cookies[i];
			if (sCookie != null) {
				if (Constant.TOKEN_NAME.equalsIgnoreCase(sCookie.getName())) {
					//System.out.println("-----------取--cookie 内的值2--------------------"+sCookie.getValue());
					return sCookie.getValue();
				}
			}
		}
		return null;
	}

	/**
	 * 获取用户Token信息
	 */
	public static XCO getUserInfo(String token) {
		XCO xco = null;
		try {
			TangYuanCache redis = CacheComponent.getInstance().getCache("cache1");
			// 获取内容
			token = URLDecoder.decode(token, "utf-8");
			String xml = (String)redis.get(Constant.SYSTEM_USER_TOKEN_PREFIX + token);
			if (null != xml) {
				xco = XCO.fromXML(xml);
			}	
			
		} catch (Throwable e) {
			logger.error("REDIS获取用户信息失败", e);
		}
		return xco;
	}

	public static String getImageVerifyKey() {
		String uuid = UUID.randomUUID().toString();
		uuid = "IV" + uuid.replaceAll("-", "").substring(0, 30);
		return uuid;
	}
}
