<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<%@ include file="../common/comm_css.jsp"%>
<head>
<title>退出</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">

</head>
<body>
	<script type="text/javascript">
	doSysExit();
	function doSysExit() {
		var xco = new XCO();
		var options = {
			url : "/userexit.xco",
			data : xco,
			success : doUserExitCallBack
		};
		$.doXcoRequest(options);
	}

	function doUserExitCallBack(data) {
		location.href="index.jsp";
	}
	</script>
</body>
</html>
